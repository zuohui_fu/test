__author__ = 'Zuohui'
import numpy as np
import numexpr as ne
from skimage.util.shape import view_as_windows
import time
import cv
import csv

# simpler data structure
(FILT, ACTV, POOL, NORM) = range(4)
(FSIZ, FNUM, FWGH) = range(3)
(AMIN, AMAX) = range(2)
(PSIZ, PORD) = range(2)
(NSIZ, NCNT, NGAN, NTHR) = range(4)

def slminit():
	
	network = []		

	layer = [[], [], [], []]
	layer[FILT][FSIZ:] = [0]
	layer[FILT][FNUM:] = [1]
	layer[ACTV][AMIN:] = [None]
	layer[ACTV][AMAX:] = [None]
	layer[POOL][PSIZ:] = [0]
	layer[POOL][PORD:] = [0]
	layer[NORM][NSIZ:] = [9]
	layer[NORM][NCNT:] = [0]
	layer[NORM][NGAN:] = [0.1]
	layer[NORM][NTHR:] = [1.0]
	network.append(layer)

	layer = [[], [], [], []]
	layer[FILT][FSIZ:] = [9]
	layer[FILT][FNUM:] = [128]
	layer[ACTV][AMIN:] = [0]
	layer[ACTV][AMAX:] = [None]
	layer[POOL][PSIZ:] = [9]
	layer[POOL][PORD:] = [2]
	layer[NORM][NSIZ:] = [5]
	layer[NORM][NCNT:] = [0]
	layer[NORM][NGAN:] = [0.1]
	layer[NORM][NTHR:] = [10.0]
	network.append(layer)
	
	layer = [[], [], [], []]
	layer[FILT][FSIZ:] = [3]
	layer[FILT][FNUM:] = [256]
	layer[ACTV][AMIN:] = [0]
	layer[ACTV][AMAX:] = [None]
	layer[POOL][PSIZ:] = [5]
	layer[POOL][PORD:] = [1]
	layer[NORM][NSIZ:] = [3]
	layer[NORM][NCNT:] = [0]
	layer[NORM][NGAN:] = [10.0]
	layer[NORM][NTHR:] = [1.0]
	network.append(layer)

	layer = [[], [], [], []]
	layer[FILT][FSIZ:] = [3]
	layer[FILT][FNUM:] = [512]
	layer[ACTV][AMIN:] = [0]
	layer[ACTV][AMAX:] = [None]
	layer[POOL][PSIZ:] = [9]
	layer[POOL][PORD:] = [10]
	layer[NORM][NSIZ:] = [5]
	layer[NORM][NCNT:] = [1]
	layer[NORM][NGAN:] = [0.1]
	layer[NORM][NTHR:] = [0.1]
	network.append(layer)
	
	np.random.seed(0)

	for i in xrange(len(network)):
		if (network[i][FILT][FSIZ] != 0):
			network[i][FILT][FWGH:] = [np.random.rand(network[i][FILT][FSIZ], network[i][FILT][FSIZ], network[i-1][FILT][FNUM], network[i][FILT][FNUM]).astype(np.float32)]
			for j in xrange(network[i][FILT][FNUM]):
				network[i][FILT][FWGH][:,:,:,j] -= np.mean(network[i][FILT][FWGH][:,:,:,j])
				network[i][FILT][FWGH][:,:,:,j] /= np.linalg.norm(network[i][FILT][FWGH][:,:,:,j])
			network[i][FILT][FWGH] = np.squeeze(network[i][FILT][FWGH])
	
	np.random.seed()

	return network

def nepow(X, O):

	if (O != 1): return ne.evaluate('X ** O')
	else:	     return X

def nediv(X, Y):
	
	if (np.ndim(X) == 2): 
		return ne.evaluate('X / Y')
	else:
		Y = Y[:,:,None]
		return ne.evaluate('X / Y')

def nemin(X, MIN): return ne.evaluate('where(X < MIN, MIN, X)')
def nemax(X, MAX): return ne.evaluate('where(X > MAX, MAX, X)')

def mcconv3(X, W):
		
	X_VAW = view_as_windows(X, W.shape[0:-1])
	Y_FPS = X_VAW.shape[0:2]
	X_VAW = X_VAW.reshape(Y_FPS[0] * Y_FPS[1], -1)
	W = W.reshape(-1, W.shape[-1])
	Y = np.dot(X_VAW, W)
	Y = Y.reshape(Y_FPS[0], Y_FPS[1], -1)

	return Y

def bxfilt2(X, F_SIZ, F_STRD):
	
	for i in reversed(xrange(2)):

		W_SIZ = np.ones(np.ndim(X))
		S_SIZ = np.ones(2)
		W_SIZ[i], S_SIZ[i] = F_SIZ, F_STRD
		X = np.squeeze(view_as_windows(X, tuple(W_SIZ)))[::S_SIZ[0], ::S_SIZ[1]] # subsampling before summation
		X = np.sum(X, -1)

	return X

def slmprop(X, network):

	for i in xrange(len(network)):
		if (network[i][FILT][FSIZ] != 0): X = mcconv3(X, network[i][FILT][FWGH])

		if (network[i][ACTV][AMIN] != None): X = nemin(X, network[i][ACTV][AMIN])
		if (network[i][ACTV][AMAX] != None): X = nemax(X, network[i][ACTV][AMAX])

		if (network[i][POOL][PSIZ] != 0):
			X = nepow(X, network[i][POOL][PORD])			
			X = bxfilt2(X, network[i][POOL][PSIZ], 2)
			X = nepow(X, (1.0 / network[i][POOL][PORD]))
		
		if (network[i][NORM][NSIZ] != 0):
			B = (network[i][NORM][NSIZ] - 1) / 2			
			X_SQS = bxfilt2(nepow(X, 2) if (np.ndim(X) == 2) else np.sum(nepow(X, 2), -1), network[i][NORM][NSIZ], 1)  			
			
			if (network[i][NORM][NCNT] == 1):
				X_SUM  = bxfilt2(X if (np.ndim(X) == 2) else np.sum(X, -1), network[i][NORM][NSIZ], 1)
				X_MEAN = X_SUM / ((network[i][NORM][NSIZ] ** 2) * network[i][FILT][FNUM])

				X = X[B:X.shape[0]-B, B:X.shape[1]-B] - X_MEAN[:,:,None]
				X_NORM = X_SQS - ((X_SUM ** 2) / ((network[i][NORM][NSIZ] ** 2) * network[i][FILT][FNUM]))
				X_NORM = X_NORM ** (1.0/2)				
			else:
				X = X[B:X.shape[0]-B, B:X.shape[1]-B]
				X_NORM = X_SQS ** (1.0/2)
			
			np.putmask(X_NORM, X_NORM < (network[i][NORM][NTHR] / network[i][NORM][NGAN]), (1/network[i][NORM][NGAN]))
			X = nediv(X, X_NORM) # numexpr for large matrix division

	return X





# def genFeatres(img_list, out_file, class_label):
#
#    # network = slminit()
#    #
#    #f = open(img_list)
#    # filenames = f.read().splitlines()
#    # f.close()
#    #
#
#
#    # for entry in filenames:
#    network = slminit()
#    image_vector = []
#    with open(img_list, 'rb') as f:
#     reader = csv.reader(f)
#     your_list = list(reader)
#     for entry in your_list:
#         if entry[2]=="0":
#
#             src1= cv.LoadImageM(entry[0])
#             src2= cv.LoadImageM(entry[1])
#             gray_full1 = cv.CreateImage(cv.GetSize(src1), 8, 1)
#             gray_full2 = cv.CreateImage(cv.GetSize(src2), 8, 1)
#             grayim1 = cv.CreateImage((200, 200), 8, 1)
#             grayim2 = cv.CreateImage((200, 200), 8, 1)
#             cv.CvtColor(src1, gray_full1, cv.CV_BGR2GRAY)
#             cv.CvtColor(src2, gray_full2, cv.CV_BGR2GRAY)
#             cv.Resize(gray_full1, grayim1, interpolation=cv.CV_INTER_CUBIC)
#             cv.Resize(gray_full2, grayim2, interpolation=cv.CV_INTER_CUBIC)
#             gray1 = cv.GetMat(grayim1)
#             gray2 = cv.GetMat(grayim2)
#
#             im_array1 = np.asarray(gray1).astype('f')
#             im_array2 = np.asarray(gray2).astype('f')
#
#           # -- compute feature map, shape [height, width, depth]
#             f_map1 = slmprop(im_array1, network)
#             f_map2 = slmprop(im_array2, network)
#             f_map=f_map1-f_map2
#
#             f_map_dims = f_map.shape
#             print "shape 1", f_map.shape
#
#             for j in range(f_map_dims[0]):
#                 for k in range(f_map_dims[1]):
#                     for l in range(f_map_dims[2]):
#                         image_vector.append(f_map[j][k][l])
#
#         vector_str = str(class_label)
#         f= open(out_file,'w')
#         for j in range(len(image_vector)):
#              vector_index = str(j + 1)
#              vector_str += " " + vector_index + ":" + str(image_vector[j])
#              f.write(vector_str)
#              f.write("\n")
def genFeatres(img_list, out_file, class_label):

   network = slminit()

   f = open(img_list)
   reader = csv.reader(f)
   filenames = list(reader)
   f.close()

   f = open(out_file,'w')

   for entry in filenames:
      #print entry

      image_vector = []
      if entry[2]=="0":

          src = cv.LoadImageM(entry[0])
          gray_full = cv.CreateImage(cv.GetSize(src), 8, 1)
          grayim = cv.CreateImage((200, 200), 8, 1)
          cv.CvtColor(src, gray_full, cv.CV_BGR2GRAY)
          cv.Resize(gray_full, grayim, interpolation=cv.CV_INTER_CUBIC)
          gray = cv.GetMat(grayim)

          im_array = np.asarray(gray).astype('f')

          # -- compute feature map, shape [height, width, depth]
          f_map1 = slmprop(im_array, network)
          #################################
          src = cv.LoadImageM(entry[1])
          gray_full = cv.CreateImage(cv.GetSize(src), 8, 1)
          grayim = cv.CreateImage((200, 200), 8, 1)
          cv.CvtColor(src, gray_full, cv.CV_BGR2GRAY)
          cv.Resize(gray_full, grayim, interpolation=cv.CV_INTER_CUBIC)
          gray = cv.GetMat(grayim)

          im_array = np.asarray(gray).astype('f')

          # -- compute feature map, shape [height, width, depth]
          f_map2 = slmprop(im_array, network)

          #################################
          f_map=(f_map1-f_map2)**2
          #################################
          f_map_dims = f_map.shape
         #print "shape 1", f_map.shape

          for j in range(f_map_dims[0]):
             for k in range(f_map_dims[1]):
                for l in range(f_map_dims[2]):
                   image_vector.append(f_map[j][k][l])

          vector_str = str(class_label)
          for j in range(len(image_vector)):
             vector_index = str(j + 1)
             vector_str += " " + vector_index + ":" + str(image_vector[j])
          #print image_vector

          f.write(vector_str)
          f.write("\n")

   f.close()


# MAIN
network = slminit()
#read line by line and generate feature





#img_list="../csvfiles/class_new_02.txt"
img_list="./csv.txt"
out_file="./feature_new_test_0.txt"
genFeatres(img_list,out_file,"-1")






#t1 = time.time()
#for i in xrange(100): ftmap = slmprop(img, network)
#t2 = time.time()
#####
#ftmap.reshape(1,ftmap.size)


#####

#print ftmap.shape
#print '%0.5f s' % (t2-t1)

